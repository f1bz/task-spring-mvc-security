package com.mykolaiv.basecamp.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mykolaiv.basecamp.spring.config.HibernateConfiguration;
import com.mykolaiv.basecamp.spring.config.MvcConfiguration;
import com.mykolaiv.basecamp.spring.config.SecurityConfiguration;
import com.mykolaiv.basecamp.spring.model.Ad;
import com.mykolaiv.basecamp.spring.model.PublishAd;
import com.mykolaiv.basecamp.spring.service.AdService;
import io.florianlopes.spring.test.web.servlet.request.MockMvcRequestBuilderUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfiguration.class, MvcConfiguration.class, SecurityConfiguration.class})
@WebAppConfiguration

public class SecurityTest {

    @Mock
    private AdService adServiceMock;

    @Autowired
    private WebApplicationContext applicationContext;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.applicationContext)
                .apply(springSecurity())
                .build();
    }

    @Test
    @WithMockUser(username = "admin")
    public void givenAdminUser_whenGetAllUsersOnAdminPage_thenOk() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/users")
                .with(user("user").password("null")
                        .authorities(new SimpleGrantedAuthority("MODIFY_USERS"))
                )
                .accept(MediaType.ALL))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("All users")));
    }

    @Test
    public void givenSimpleUser_whenGetAllUsersOnAdminPage_thenFail() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/admin/users")
                .with(user("user"))
                .accept(MediaType.ALL))
                .andExpect(status().is(500));
    }

    @Test
    @Ignore
    public void givenUser_whenPostNewAd_thenRedirect() throws Exception {
        Ad ad = new Ad();
        ad.setId(0);
        when(adServiceMock.add(any())).thenReturn(ad);

        PublishAd publishAd = new PublishAd();
        publishAd.setTitle("123123123213");
        publishAd.setDescription("123123213213");
        publishAd.setPrice(123);

        mockMvc.perform(MockMvcRequestBuilderUtils.postForm("/ads/publish", publishAd)
                .with(user("user"))
                .accept(MediaType.ALL))
                .andExpect(MockMvcResultMatchers.redirectedUrl("/ads/0"));

        verify(adServiceMock, times(1)).add(any());
    }

    @Test
    public void givenNoUser_whenPostNewAd_thenEmptyResponse() throws Exception {
        PublishAd publishAd = new PublishAd();
        publishAd.setTitle("123123123213");
        publishAd.setDescription("123123213213");
        publishAd.setPrice(123);

        mockMvc.perform(MockMvcRequestBuilderUtils.postForm("/ads/publish", publishAd)
                .accept(MediaType.ALL))
                .andExpect(redirectedUrl(null))
                .andExpect(content().string(equalTo("")));
    }

    public static String asJsonString(final Object obj) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}