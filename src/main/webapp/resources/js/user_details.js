$('document').ready(function () {
    $('.save-btn').click(function () {
        var login = $("[name=login]").val();
        var name = $("[name=name]").val();
        var balance = $("[name=balance]").val();
        var password = $("[name=password]").val();
        var url = $("#edit-link").attr('href');
        editUser(url, login, name, balance, password);
    });
});

function editUser(url, login, name, balance, password) {
    $.ajax({
        url: url,
        type: 'POST',
        data: {
            login: login,
            name: name,
            balance: balance,
            password: password,
        },
        success: function (result) {
            changeStatus(result);
        }, error: function (xhr, ajaxOptions, thrownError) {
            showError();
        }
    });
}

function changeStatus(result) {
    var status = result['success'];
    var description = result['description'];
    $('.edit-result-label').fadeOut();
    if (status) {
        $('.edit-result-label').removeClass("text-danger");
        $('.edit-result-label').addClass("text-success");
    } else {
        $('.edit-result-label').removeClass("text-success");
        $('.edit-result-label').addClass("text-danger");
    }
    $('.edit-result-label').fadeOut();
    $('.edit-result-label').html(description);
    $('.edit-result-label').fadeIn();
}

function showError() {
    $('.edit-result-label').removeClass("text-success");
    $('.edit-result-label').addClass("text-danger");
    $('.edit-result-label').fadeOut();
    $('.edit-result-label').html('Oops..something went wrong!');
    $('.edit-result-label').fadeIn();
}