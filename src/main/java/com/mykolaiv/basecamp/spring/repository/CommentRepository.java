package com.mykolaiv.basecamp.spring.repository;

import com.mykolaiv.basecamp.spring.model.Comment;

import java.util.List;

public interface CommentRepository {

    List<Comment> getAll();

    void add(Comment comment);

    void update(Comment comment);

    void delete(Comment comment);

    Comment getById(int id);

    List<Comment> getByUserId(int id);

    List<Comment> getByAdId(int id);
}
