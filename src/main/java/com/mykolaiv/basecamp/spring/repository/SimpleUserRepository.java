package com.mykolaiv.basecamp.spring.repository;

import com.mykolaiv.basecamp.spring.model.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class SimpleUserRepository implements UserRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional
    @SuppressWarnings(value = "unchecked")
    public List<User> getOnPage(int page, int rowsPerPage) {
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(User.class);
        criteria.setFirstResult(page);
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        criteria.setMaxResults(rowsPerPage);
        return (List<User>) criteria.list().stream().distinct().collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void add(User user) {
        entityManager.persist(user);
        entityManager.flush();
    }

    @Override
    @Transactional
    public void edit(User user) {
        entityManager.merge(user);
        entityManager.flush();
    }

    @Override
    @Transactional
    public long getAmountOfUsers() {
        return (Long) entityManager.createQuery("SELECT count(*) FROM User").getSingleResult();
    }

    @Override
    @Transactional
    public User getById(int id) {
        return getUserByField("id", String.valueOf(id));
    }

    @Override
    @Transactional
    public User getByLogin(String login) {
        return getUserByField("login", login);
    }

    @Transactional
    public User getUserByField(String field, String value) {
        Session session = entityManager.unwrap(Session.class);
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        Root<User> root = criteriaQuery.from(User.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get(field), value));
        Query<User> query = session.createQuery(criteriaQuery);
        List<User> resultList = query.getResultList();
        if (resultList.isEmpty()) {
            return null;
        }
        return resultList.get(0);
    }
}
