package com.mykolaiv.basecamp.spring.repository;

import com.mykolaiv.basecamp.spring.model.security.Privilege;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class SimplePrivilegeRepository implements PrivilegeRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Privilege getByName(String name) {
        Session session = entityManager.unwrap(Session.class);
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Privilege> criteriaQuery = criteriaBuilder.createQuery(Privilege.class);
        Root<Privilege> root = criteriaQuery.from(Privilege.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("name"), name));
        Query<Privilege> query = session.createQuery(criteriaQuery);
        List<Privilege> resultList = query.getResultList();
        if (resultList.isEmpty()) {
            return null;
        }
        return resultList.get(0);
    }

    @Override
    @Transactional
    public void add(Privilege privilege) {
        entityManager.persist(privilege);
    }
}