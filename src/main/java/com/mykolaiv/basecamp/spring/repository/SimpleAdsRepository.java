package com.mykolaiv.basecamp.spring.repository;

import com.mykolaiv.basecamp.spring.model.Ad;
import com.mykolaiv.basecamp.spring.model.AdFilter;
import com.mykolaiv.basecamp.spring.model.User;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.Instant;
import java.util.List;

@Repository
public class SimpleAdsRepository implements AdsRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @SuppressWarnings(value = "unchecked")
    @Transactional
    public List<Ad> getAllFiltered(AdFilter adFilter) {
        String status = adFilter.getStatus();
        Integer maxPrice = adFilter.getMaxPrice();
        Integer minPrice = adFilter.getMinPrice();
        String title = StringUtils.stripToNull(adFilter.getTitle());
        String description = StringUtils.stripToNull(adFilter.getDescription());
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Ad.class);
        if (title != null) {
            criteria.add(Restrictions.like("title", title, MatchMode.ANYWHERE));
        }
        if (description != null) {
            criteria.add(Restrictions.like("description", description, MatchMode.ANYWHERE));
        }
        if (status!= null && (status.contains("SOLD") || status.contains("SELLING"))) {
            if (status.equals("SOLD")) {
                criteria.add(Restrictions.isNotNull("buyer"));
            } else {
                criteria.add(Restrictions.isNull("buyer"));
            }
        }
        if (maxPrice != null) {
            criteria.add(Restrictions.le("price", maxPrice));
        }
        if (minPrice != null) {
            criteria.add(Restrictions.ge("price", minPrice));
        }
        return (List<Ad>) criteria.list();
    }

    @Override
    @Transactional
    public Ad save(Ad ad) {
        entityManager.persist(ad);
        entityManager.flush();
        return ad;
    }

    @Override
    @Transactional
    public Ad getById(int id) {
        List<Ad> ads = getAdByField("id", String.valueOf(id));
        if (ads.isEmpty()) {
            return null;
        }
        return ads.get(0);
    }

    @Override
    public void delete(Ad ad) {
        ad = entityManager.contains(ad) ? ad : entityManager.merge(ad);
        entityManager.remove(ad);
    }

    @Override
    public void buy(Ad ad, User buyer, Instant timestamp) {
        if (ad.getBuyer() == null) {
            ad.setBuyer(buyer);
            ad.setBuyingTimestamp(timestamp);
            entityManager.merge(ad);
            entityManager.flush();
        }
    }

    @Transactional
    public List<Ad> getAdByField(String field, String value) {
        Session session = entityManager.unwrap(Session.class);
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Ad> criteriaQuery = criteriaBuilder.createQuery(Ad.class);
        Root<Ad> root = criteriaQuery.from(Ad.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get(field), value));
        Query<Ad> query = session.createQuery(criteriaQuery);
        return query.getResultList();
    }
}