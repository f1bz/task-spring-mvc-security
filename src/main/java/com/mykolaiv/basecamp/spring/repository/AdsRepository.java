package com.mykolaiv.basecamp.spring.repository;

import com.mykolaiv.basecamp.spring.model.Ad;
import com.mykolaiv.basecamp.spring.model.AdFilter;
import com.mykolaiv.basecamp.spring.model.User;

import java.time.Instant;
import java.util.List;

public interface AdsRepository {

    List<Ad> getAllFiltered(AdFilter adFilter);

    Ad save(Ad ad);

    Ad getById(int id);

    void delete(Ad ad);

    void buy(Ad ad, User buyer, Instant now);
}
