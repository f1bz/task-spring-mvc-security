package com.mykolaiv.basecamp.spring.repository;

import com.mykolaiv.basecamp.spring.model.User;

import java.util.List;

public interface UserRepository {

    List<User> getOnPage(int page, int rowsPerPage);

    void add(User user);

    void edit(User user);

    long getAmountOfUsers();

    User getById(int id);

    User getByLogin(String login);
}
