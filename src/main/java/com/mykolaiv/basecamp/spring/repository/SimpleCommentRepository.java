package com.mykolaiv.basecamp.spring.repository;

import com.mykolaiv.basecamp.spring.model.Comment;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class SimpleCommentRepository implements CommentRepository {
    
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @SuppressWarnings(value = "unchecked")
    @Transactional
    public List<Comment> getAll() {
        Session session = entityManager.unwrap(Session.class);
        Criteria criteria = session.createCriteria(Comment.class);
        return (List<Comment>) criteria.list();
    }

    @Override
    @Transactional
    public void add(Comment comment) {
        entityManager.persist(comment);
        entityManager.flush();
    }

    @Override
    @Transactional
    public void update(Comment comment) {
        entityManager.persist(comment);
        entityManager.flush();
    }

    @Override
    public void delete(Comment comment) {
        entityManager.remove(comment);
    }

    @Override
    @Transactional
    public Comment getById(int id) {
        List<Comment> comments = getCommentByField("id", String.valueOf(id));
        if (comments.isEmpty()) {
            return null;
        }
        return comments.get(0);
    }

    @Override
    @Transactional
    public List<Comment> getByUserId(int id) {
        return getCommentByField("user_id", String.valueOf(id));
    }

    @Override
    @Transactional
    public List<Comment> getByAdId(int id) {
        return getCommentByField("ad_id", String.valueOf(id));
    }

    @Transactional
    public List<Comment> getCommentByField(String field, String value) {
        Session session = entityManager.unwrap(Session.class);
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Comment> criteriaQuery = criteriaBuilder.createQuery(Comment.class);
        Root<Comment> root = criteriaQuery.from(Comment.class);
        criteriaQuery.select(root).where(criteriaBuilder.equal(root.get(field), value));
        Query<Comment> query = session.createQuery(criteriaQuery);
        return query.getResultList();
    }
}