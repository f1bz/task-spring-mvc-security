package com.mykolaiv.basecamp.spring.repository;

import com.mykolaiv.basecamp.spring.model.security.Role;

public interface RoleRepository {

    Role getByName(String name);

    void add(Role role);
}