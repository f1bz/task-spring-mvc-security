package com.mykolaiv.basecamp.spring.repository;

import com.mykolaiv.basecamp.spring.model.security.Privilege;

public interface PrivilegeRepository {

    Privilege getByName(String name);

    void add(Privilege privilege);
}