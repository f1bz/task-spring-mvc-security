package com.mykolaiv.basecamp.spring.controller.admin;

import com.mykolaiv.basecamp.spring.model.OperationResult;
import com.mykolaiv.basecamp.spring.model.User;
import com.mykolaiv.basecamp.spring.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.util.List;

@Slf4j
@Validated
@Controller
@RequestMapping("/admin/users")
@PreAuthorize("hasAuthority('MODIFY_USERS')")
public class AllUsersController {

    private UserService userService;

    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public AllUsersController(UserService userService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userService = userService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostMapping
    public String post() {
        return "redirect:users";
    }

    @GetMapping
    public String getAllUsers(@RequestParam(name = "page", defaultValue = "0") int page, Model model) {
        List<User> users = userService.getOnPage(page * UserService.ROWS_PER_PAGE);
        long total = userService.getAmountOfUsers();
        model.addAttribute("users", users);
        if (page != 0) {
            model.addAttribute("previousPage", page - 1);
        }
        if ((page + 1) * UserService.ROWS_PER_PAGE < total) {
            model.addAttribute("nextPage", page + 1);
        }
        return "users/admin/all_users.html";
    }

    @GetMapping(value = "/{id}")
    public String userDetails(@PathVariable(value = "id") int id, Model model) {
        User user = userService.getById(id);
        if (user != null) {
            model.addAttribute("user", user);
            return "/users/admin/user_details.html";
        } else {
            return "users/user_not_found.html";
        }
    }

    @PostMapping(value = "/edit/{id}", produces = {"application/json"})
    public @ResponseBody
    OperationResult updateUser(@PathVariable(value = "id") int id,
                               @Size(min = 1) @RequestParam(required = false, name = "name") String name,
                               @RequestParam(required = false, name = "password") String password,
                               @Min(0) @RequestParam(required = false, name = "balance") Integer balance) {
        String responseDescription = "User was successfully edited!";
        boolean edited = true;
        User user = userService.getById(id);
        if (StringUtils.isNotBlank(name)) {
            user.setName(name);
        }
        if (StringUtils.isNotBlank(password)) {
            user.setPassword(bCryptPasswordEncoder.encode(password));
        }
        if (balance != null) {
            user.setBalance(balance);
        }
        try {
            userService.update(user);
        } catch (Exception e) {
            edited = false;
            responseDescription = e.getMessage();
            log.error("{0}", e);
        }
        return new OperationResult(edited, responseDescription);
    }
}