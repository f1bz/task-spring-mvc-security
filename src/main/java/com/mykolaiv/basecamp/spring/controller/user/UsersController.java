package com.mykolaiv.basecamp.spring.controller.user;

import com.mykolaiv.basecamp.spring.model.User;
import com.mykolaiv.basecamp.spring.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Controller
@RequestMapping("/users")
public class UsersController {

    private UserService userService;

    public UsersController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/{id}")
    public String userDetails(@PathVariable(value = "id") int id, Model model) {
        User user = userService.getById(id);
        if (user == null) {
            return "users/user_not_found.html";
        }
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        User loggedUser = userService.getByLogin(authentication.getName());
        if (!authentication.getName().equals("anonymousUser") && loggedUser.getId().equals(user.getId())) {
            return "redirect:/profile";
        }
        model.addAttribute("user", user);
        return "/users/user_details.html";
    }

}