package com.mykolaiv.basecamp.spring.controller.app;

import com.mykolaiv.basecamp.spring.config.SecurityConfiguration;
import com.mykolaiv.basecamp.spring.model.User;
import com.mykolaiv.basecamp.spring.model.UserDTO;
import com.mykolaiv.basecamp.spring.service.RoleService;
import com.mykolaiv.basecamp.spring.service.UserService;
import com.mykolaiv.basecamp.spring.service.validation.UserValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;

@Slf4j
@Controller
@RequestMapping("/signup")
public class SignUpController {

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private SecurityConfiguration securityConfiguration;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @GetMapping
    public String getSignUpPage(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return "redirect:/profile";
        }
        UserDTO userDto = new UserDTO();
        model.addAttribute("user", userDto);
        return "signup.html";
    }

    @PostMapping
    public String trySignUp(@ModelAttribute("user") @Validated UserDTO userDTO,
                            Model model, BindingResult result) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            return "redirect:/profile";
        }
        userValidator.validate(userDTO, result);
        if (result.hasErrors()) {
            if (result.getFieldError("login") != null) {
                model.addAttribute("badLogin", true);
            }
            if (result.getFieldError("password") != null || result.getFieldError("confirmPassword") != null ) {
                model.addAttribute("badPassword", true);
            }
            if (result.getFieldError("name") != null) {
                model.addAttribute("badName", true);
            }
            return "signup.html";
        }

        User newUser = new User();
        String password = securityConfiguration.getBCryptPasswordEncoder().encode(userDTO.getPassword());
        newUser.setPassword(password);
        newUser.setLogin(userDTO.getLogin());
        newUser.setName(userDTO.getName());
        newUser.setBalance(1000);
        newUser.setRoles(Collections.singleton(roleService.getByName("USER")));
        userService.add(newUser);
        securityConfiguration.performLogin(userDTO.getLogin(), userDTO.getPassword());
        return "redirect:/profile";
    }
}