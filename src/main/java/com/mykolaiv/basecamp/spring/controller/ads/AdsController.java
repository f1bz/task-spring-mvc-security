package com.mykolaiv.basecamp.spring.controller.ads;

import com.mykolaiv.basecamp.spring.model.Ad;
import com.mykolaiv.basecamp.spring.model.AdFilter;
import com.mykolaiv.basecamp.spring.model.Comment;
import com.mykolaiv.basecamp.spring.model.PublishAd;
import com.mykolaiv.basecamp.spring.model.User;
import com.mykolaiv.basecamp.spring.service.AdService;
import com.mykolaiv.basecamp.spring.service.CommentService;
import com.mykolaiv.basecamp.spring.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.time.Instant;
import java.util.List;

@Slf4j
@Controller
@Validated
@RequestMapping("/ads")
public class AdsController {

    private CommentService commentService;

    private UserService userService;

    private AdService adService;

    public AdsController(CommentService commentService, UserService userService, AdService adService) {
        this.commentService = commentService;
        this.userService = userService;
        this.adService = adService;
    }

    @GetMapping
    public String getAllAds(AdFilter adFilter, Model model) {
        List<Ad> filteredAds = adService.getAllFiltered(adFilter);
        model.addAttribute("ads", filteredAds);

        model.addAttribute("status", "ALL");
        model.addAttribute("title", "");
        model.addAttribute("description", "");
        model.addAttribute("maxPrice", 1000);
        model.addAttribute("minPrice", 0);

        if (adFilter.getStatus() != null) {
            model.addAttribute("status", adFilter.getStatus());
        }
        if (adFilter.getDescription() != null) {
            model.addAttribute("description", adFilter.getDescription());
        }
        if (adFilter.getMaxPrice() != null) {
            model.addAttribute("maxPrice", adFilter.getMaxPrice());
        }
        if (adFilter.getMinPrice() != null) {
            model.addAttribute("minPrice", adFilter.getMinPrice());
        }
        if (adFilter.getTitle() != null) {
            model.addAttribute("title", adFilter.getTitle());
        }
        return "ads/all_ads.html";
    }

    @Transactional
    @GetMapping(value = "/{id}")
    public String adDetails(@RequestParam(value = "error", required = false) String errorMessage,
                            @PathVariable(value = "id") int id, Model model) {
        if (errorMessage != null) {
            model.addAttribute("error_comment", errorMessage);
        }
        Ad ad = adService.getById(id);
        if (ad != null) {
            model.addAttribute("ad", ad);
            return "ads/ad_details.html";
        } else {
            return "ads/ad_not_found.html";
        }
    }

    @Transactional
    @PostMapping(value = "/{id}/publish-comment")
    public String publishComment(@Size(min = 1)  @RequestParam(value = "value") String commentMessage,
                                 @PathVariable(value = "id") int id){
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        User loggedUser = userService.getByLogin(authentication.getName());
        if (loggedUser != null) {
            Comment comment = new Comment();
            comment.setUser(loggedUser);
            comment.setAd(adService.getById(id));
            comment.setValue(commentMessage);
            comment.setTimestamp(Instant.now());
            commentService.save(comment);
        }
        return "redirect:/ads/" + id;
    }

    @PostMapping(value = "/{adId}/edit-comment/{id}")
    @Transactional
    public String editComment(@Size(min = 1)  @RequestParam(value = "value") String commentMessage,
                              @PathVariable(value = "id") int id,
                              @PathVariable(value = "adId") int adId){
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        User loggedUser = userService.getByLogin(authentication.getName());
        Comment comment = commentService.getById(id);
        if (loggedUser != null && comment != null
                && comment.getUser().getLogin().equals(loggedUser.getLogin())) {
            comment.setValue(commentMessage);
            commentService.update(comment);
        }
        return "redirect:/ads/" + adId;
    }

    @Transactional
    @GetMapping(value = "/{id}/buy")
    public String buyAd(@PathVariable(value = "id") int id) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        User loggedUser = userService.getByLogin(authentication.getName());
        Ad ad = adService.getById(id);
        if (loggedUser != null && !ad.getSeller().getLogin().equals(loggedUser.getLogin())) {
            adService.buy(ad, loggedUser);
        }
        return "redirect:/profile";
    }

    @Transactional
    @GetMapping(value = "/publish")
    public String publish() {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        User loggedUser = userService.getByLogin(authentication.getName());
        if (loggedUser != null) {
            return "ads/ad_publish.html";
        }
        return "redirect:/profile";
    }

    @Transactional
    @GetMapping(value = "{id}/edit")
    public String edit(@PathVariable(value = "id") int id, Model model) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        User loggedUser = userService.getByLogin(authentication.getName());
        Ad ad = adService.getById(id);
        if (loggedUser != null && ad != null && ad.getBuyer() == null && ad.getSeller().getLogin().equals(loggedUser.getLogin())) {
            model.addAttribute("ad", ad);
            return "ads/ad_publish.html";
        }
        return "redirect:/profile";
    }

    @Transactional
    @PostMapping(value = "/publish")
    public String publishFromForm(@Valid PublishAd publishAd, BindingResult bindingResult, HttpServletResponse resp) throws IOException {
        if (bindingResult.hasErrors()) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Bad params!");
            return null;
        }
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        User loggedUser = userService.getByLogin(authentication.getName());
        if (loggedUser != null) {
            Ad ad = publishAd.getId() != null ? adService.getById(publishAd.getId()) : new Ad();
            ad.setSeller(loggedUser);
            ad.setPrice(publishAd.getPrice());
            ad.setTitle(publishAd.getTitle());
            ad.setDescription(publishAd.getDescription());
            ad = adService.add(ad);
            return "redirect:/ads/" + ad.getId();
        } else {
            resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, "User is not authorized!");
            return null;
        }
    }

    @Transactional
    @GetMapping(value = "/{id}/delete")
    public String deleteAd(@PathVariable(value = "id") int id) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        User loggedUser = userService.getByLogin(authentication.getName());
        Ad ad = adService.getById(id);
        boolean isAdmin = loggedUser.getRoles()
                .stream()
                .anyMatch(x -> x.getName().equals("ADMIN"));
        if (loggedUser != null && ad.getBuyer() == null
                && (ad.getSeller().getLogin().equals(loggedUser.getLogin()) || isAdmin)) {
            adService.delete(ad);
        }
        return "redirect:/profile";
    }

    @GetMapping(value = "{adId}/delete-comment/{id}")
    @Transactional
    public String deleteComment(@PathVariable(value = "id") int id, @PathVariable(value = "adId") int adId) {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        Authentication authentication = securityContext.getAuthentication();
        User loggedUser = userService.getByLogin(authentication.getName());
        Comment commentToDelete = commentService.getById(id);
        boolean isAdmin = loggedUser.getRoles()
                .stream()
                .anyMatch(x -> x.getName().equals("ADMIN"));
        if (loggedUser != null && commentToDelete != null
                && (commentToDelete.getUser().getLogin().equals(loggedUser.getLogin()) || isAdmin)) {
            commentService.deleteById(commentToDelete);
        }
        return "redirect:/ads/" + adId;
    }
}