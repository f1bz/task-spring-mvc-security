package com.mykolaiv.basecamp.spring.service;

import com.mykolaiv.basecamp.spring.model.security.Privilege;
import com.mykolaiv.basecamp.spring.repository.PrivilegeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class PrivilegeService {

    @Autowired
    private PrivilegeRepository privilegeRepository;

    @Transactional
    public void add(Privilege privilege) {
        privilegeRepository.add(privilege);
    }

    @Transactional
    public Privilege getByLogin(String name) {
        return privilegeRepository.getByName(name);
    }
}
