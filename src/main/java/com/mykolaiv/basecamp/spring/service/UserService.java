package com.mykolaiv.basecamp.spring.service;

import com.mykolaiv.basecamp.spring.model.User;
import com.mykolaiv.basecamp.spring.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService {

    public static final int ROWS_PER_PAGE = 10;

    private UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public List<User> getOnPage(int page) {
        return userRepository.getOnPage(page, ROWS_PER_PAGE);
    }

    @Transactional
    public long getAmountOfUsers() {
        return userRepository.getAmountOfUsers();
    }

    @Transactional
    public void add(User user) {
        userRepository.add(user);
    }

    @Transactional
    public void update(User user) {
        userRepository.edit(user);
    }

    @Transactional
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Transactional
    public User getByLogin(String login) {
        return userRepository.getByLogin(login);
    }
}
