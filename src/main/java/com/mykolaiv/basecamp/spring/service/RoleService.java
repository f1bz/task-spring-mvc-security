package com.mykolaiv.basecamp.spring.service;

import com.mykolaiv.basecamp.spring.model.security.Role;
import com.mykolaiv.basecamp.spring.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Transactional
    public void add(Role role) {
        roleRepository.add(role);
    }

    @Transactional
    public Role getByName(String name) {
        return roleRepository.getByName(name);
    }
}
