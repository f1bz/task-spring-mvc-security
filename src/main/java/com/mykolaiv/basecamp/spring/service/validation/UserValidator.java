package com.mykolaiv.basecamp.spring.service.validation;

import com.mykolaiv.basecamp.spring.model.User;
import com.mykolaiv.basecamp.spring.model.UserDTO;
import com.mykolaiv.basecamp.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz == UserDTO.class;
    }

    @Override
    public void validate(Object target, Errors errors) {
        UserDTO userDTO = (UserDTO) target;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "NotEmpty.UserDTO.login");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.UserDTO.password");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmPassword", "NotEmpty.UserDTO.confirmPassword");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "NotEmpty.UserDTO.Name");

        User user = userService.getByLogin(userDTO.getLogin());
        if (user != null) {
            errors.rejectValue("login", "Duplicate.UserDTO.login");
        }

        if (!errors.hasErrors()) {
            if (!userDTO.getConfirmPassword().equals(userDTO.getPassword())) {
                errors.rejectValue("confirmPassword", "Match.UserDTO.confirmPassword");
            }
        }
    }
}
