package com.mykolaiv.basecamp.spring.service.validators;

import com.mykolaiv.basecamp.spring.model.constraints.AdTextValueConstraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AdTextValueValidator implements ConstraintValidator<AdTextValueConstraint, String> {

    @Override
    public void initialize(AdTextValueConstraint valueConstraint) {
    }

    @Override
    public boolean isValid(String field, ConstraintValidatorContext cxt) {
        return field != null && field.length() > 8;
    }

}
