package com.mykolaiv.basecamp.spring.service;

import com.mykolaiv.basecamp.spring.model.Ad;
import com.mykolaiv.basecamp.spring.model.AdFilter;
import com.mykolaiv.basecamp.spring.model.User;
import com.mykolaiv.basecamp.spring.repository.AdsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
public class AdService {

    @Autowired
    private AdsRepository adsRepository;

    @Autowired
    private UserService userService;

    @Transactional
    public List<Ad> getAllFiltered(AdFilter adFilter) {
        return adsRepository.getAllFiltered(adFilter);
    }

    @Transactional
    public Ad add(Ad ad) {
        return adsRepository.save(ad);
    }

    @Transactional
    public Ad getById(int id) {
        return adsRepository.getById(id);
    }

    @Transactional
    public void delete(Ad ad) {
        adsRepository.delete(ad);
    }

    @Transactional
    public void buy(Ad ad, User buyer) {
        if (buyer.getBalance() >= ad.getPrice()) {
            adsRepository.buy(ad, buyer, Instant.now());
            User seller = ad.getSeller();
            seller.setBalance(seller.getBalance() + ad.getPrice());
            buyer.setBalance(buyer.getBalance() - ad.getPrice());
            userService.update(buyer);
            userService.update(seller);
        }else{
            throw new IllegalArgumentException("not enough money!");
        }
    }
}