package com.mykolaiv.basecamp.spring.service.handlers;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SimpleAuthenticationFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
            throws IOException {
        if (exception.getMessage().contains("Bad credentials")) {
            request.setAttribute("message", "User doesn't exist or wrong password!");
        }
        response.sendRedirect("/login?error");
    }
}
