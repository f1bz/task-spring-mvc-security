package com.mykolaiv.basecamp.spring.service;

import com.mykolaiv.basecamp.spring.model.Comment;
import com.mykolaiv.basecamp.spring.repository.CommentRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CommentService {

    private CommentRepository commentRepository;

    public CommentService(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Transactional
    public void save(Comment comment) {
        commentRepository.add(comment);
    }

    @Transactional
    public void update(Comment comment) {
        commentRepository.update(comment);
    }

    @Transactional
    public void deleteById(Comment comment) {
        commentRepository.delete(comment);
    }

    @Transactional
    public Comment getById(int id) {
        return commentRepository.getById(id);
    }

}