package com.mykolaiv.basecamp.spring.model;

import lombok.Data;

@Data
public class AdFilter {

    private String title;
    private String description;
    private Integer minPrice;
    private Integer maxPrice;
    private String status;

}
