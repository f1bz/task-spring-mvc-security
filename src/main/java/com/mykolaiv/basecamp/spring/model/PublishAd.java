package com.mykolaiv.basecamp.spring.model;

import com.mykolaiv.basecamp.spring.model.constraints.AdTextValueConstraint;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class PublishAd {

    private Integer id;

    @AdTextValueConstraint
    private String title;

    @AdTextValueConstraint
    private String description;

    @NotNull
    @Min(1)
    private int price;

}
