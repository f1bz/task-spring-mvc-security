package com.mykolaiv.basecamp.spring.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OperationResult {
    private boolean success;
    private String description;
}
